
## Meta Characters:
- `^` Starts
- `$` ends

### `[]` Square brackets.
- `[abc]` will match either a, b or c
- `[^abc]` will match characters other than a, b or c.
- `[1-6a-d]` can specify the range of charaters.

- `.` will match any character excepe `\n`
- `^ab` The string will starts with ab
- `bc$` The string ends with $
- `*` means zero or more occurrences left to it.
- `+` means one or more occurrences left to it.
- `?` means zero or one occurrences left to it.
- `{n,m}` This means at least n, and at most m repetitions of the pattern left to it.

### `|` - Alternation
- Vertical bar `|` is used for alternation (or operator).

### `()` GRoup
- It is used to group sub patterns
- For example `(a|b|c)xz`

### `\` Backslash
- `\` is used to escape various characters including all the metacharacters.

- `\Athe` will match the charaters are at the start of a string.
- For example: 'the sun' 🟢
-  `In the sun`: 🔴

- `\b` - Matches if the specified characters are at the beginning or end of a word.
	- `\bfoo` Starts with foo.
	-  `foo\b` ends with foo.

- `\B` - Opposite of `\b`. Matches if the specified characters are not at the beginning or end of a word.
- `\d` => `[0-9]`
- `\D` => `[^0-9]`
- `\s` - Matches where a string contains any whitespace character. Equivalent to `[ \t\n\r\f\v]`
- `\S` - Matches where a string contains any non-whitespace character. Equivalent to `[^ \t\n\r\f\v]`
- `\w` - Matches any alphanumeric character (digits and alphabets). Equivalent to `[a-zA-Z0-9_]`. By the way, underscore `_` is also considered an alphanumeric character.
- `\W` - Matches any non-alphanumeric character. Equivalent to `[^a-zA-Z0-9_]`
- `\Z` - Matches if the specified characters are at the end of a string.
	- For example: `Python\Z` => `I like Python`

Difference between re.match() and re.search()
```
re.search() vs re.match() – 
There is a difference between the use of both functions. Both return the first match of a substring found in the string, but re.match() searches only from the beginning of the string and return match object if found. But if a match of substring is found somewhere in the middle of the string, it returns none. 
While re.search() searches for the whole string even if the string contains multi-lines and tries to find a match of the substring in all the lines of string

```
- re.match()
- re.search()
- re.findall()
- re.sub(pattern, replace, string)
- re.split()
- re.split(r'bab\b', 'sdbab ahadahbab', 1)
	- Split at the first match.

### What's raw string.
```
When r or R prefix is used before a regular expression, it means raw string. For example, '\n' is a new line whereas r'\n' means two characters: a backslash \ followed by n
```