

def say_name(name):

    def say():
        print(name)
    
    return say



def product(i):

    def multiply(j):
        return i * j
    
    return multiply

# a = async_to_sync()(
#     {
#         "name": "vivek"
#     }
# )