## Learning about os module.

1. os.getcwd() 
   1. Current working directory.
2. os.chdir(path: str)
3. os.listdir()
4. os.mkdir(directory_name: str)
5. os.rename(previous_name: str, new_name: str)
6. os.remove(path: str)
7. os.rmdir()
8. shutil.rmtree() To remove a non empty directory.