"""
Perform CRUD operations.
"""
import json

db_name = "./db.json"

def create_record():
    with open(db_name, 'r') as db:
        data = json.load(db)
    
    id = max([ x['id'] for x in data ]) + 1
    fn = input("First Name: ")
    ln = input("Last Name: ")
    email = input("Email: ")
    gender = input("Gender: ")
    ip = input("IP Address: ")

    entry =     {
        "id": id,
        "first_name": fn,
        "last_name": ln,
        "email": email,
        "gender": gender,
        "ip_address": ip
    }

    data.append(entry)

    with open(db_name, 'w') as db:
        json.dump(data, db)


def update_record():
    input_id = int(input("Enter your ID: "))

    with open(db_name, 'r') as db:
        data = json.load(db)
    
    found = False
    for entry in data:
        if entry['id'] == input_id:
            found = True
            fn = input("First Name: ")
            ln = input("Last Name: ")
            email = input("Email: ")
            gender = input("Gender: ")
            ip = input("IP Address: ")

            entry['first_name'] = fn
            entry['last_name'] = ln
            entry['email'] = email
            entry['gender'] = gender
            entry['ip_address'] = ip

            with open(db_name, 'w') as db:
                json.dump(data, db)
            print("Changes Updated Succesfully.")

    
    if not found:
        print("Ohhh! You're entering a wrong id.")

def delete_record():
    input_id = int(input("Enter your ID: "))

    with open(db_name, 'r') as db:
        data = json.load(db)
    
    found = False
    for entry in data:
        if entry['id'] == input_id:
            found = True
            # When you find the id.
            data.remove(entry)
            with open(db_name, 'w') as db:
                json.dump(data, db)
            print("Removed successfully.")

    if not found:
        print("Ohhh! You're entering a wrong id.")


def retrieve_record():
    input_id = int(input("Enter your ID: "))

    with open(db_name, 'r') as db:
        data = json.load(db)
    
    found = False
    for entry in data:
        if entry['id'] == input_id:
            found = True
            # When you find the id.
            print('Id:', entry['id'])
            print('first_name:', entry['first_name'])
            print('last_name:', entry['last_name'])
            print('email:', entry['email'])
            print('gender:', entry['gender'])
            print('IP:', entry['ip_address'])

    if not found:
        print("Ohhh! You're entering a wrong id.")


def main():
    print("""
    1. Create a new record.
    2. Retrieve the Record.
    3. Update the Record.
    4. Delete the record.
    """)
    ch = int(input(">>> "))

    if ch == 1:
        create_record()
    elif ch == 2:
        retrieve_record()
    elif ch == 3:
        update_record()
    elif ch == 4:
        delete_record()

if __name__ == '__main__':
    main()