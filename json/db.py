
import json



def render_json():
    file_name = "./db.json"

    with open(file_name, 'r') as file:
        data = json.load(file)

    return data

if __name__ == "__main__":
    data = {
        'firstName': "Vivek",
        'secondName': "Kumar",
        'data': (
            'name',
            'age',
            'etc.'
        )
    }

    output_file = "output.json"

    with open(output_file, "w") as file:
        json.dump(data, file)
    
