# What is JSON ?
"""
Javascript Object Notation.

"""
# Why do we need it ?
import json

{
    "name": {
        "first": [
            'albert',
            'gt',
        ],
    },
    "is_active": False
}

# Serialization
data = """
{
    "name": "vivek",
    "age": null
}
"""
json.loads(data)
# DeSerialization


data = {
    "name": "Vivek",
    "Age": "Vivek",
    "is_active": ('No', False)
}