import requests


def download_file():
    url = "https://jsonplaceholder.typicode.com/comments"

    param = {
        'postId': 10
    }

    r = requests.get(url, params=param)

    with open('dracula.html', "wb") as file:
        data = r.content
        file.write(data)


def send_post():
    title = input("Enter Title: ")
    body = input("Enter body: ")
    user_id = int(input("Enter User Id: "))

    user_agent = "Mozilla/5.0 (X11; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0"

    url = "https://jsonplaceholder.typicode.com/posts"

    headers = {
        'User-Agent': user_agent
    }
    data = {
        'title': title,
        'body': 'body',
        'userId': user_id
    }

    r = requests.post(url, data=data, headers=headers)

    if r.status_code in [200, 201]:
        print("Status Code: ", r.status_code)
        data = r.json()
        print(data)


# send_post()

def send_put():
    title = input("Enter Title: ")
    body = input("Enter body: ")
    user_id = int(input("Enter User Id: "))
    id =1
    user_agent = "Mozilla/5.0 (X11; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0"

    url = f"https://jsonplaceholder.typicode.com/posts/{id}"

    headers = {
        'User-Agent': user_agent
    }
    data = {
        'title': title,
        'body': body,
        'userId': user_id,
        'id': id
    }

    r = requests.put(url, data=data, headers=headers)
    print(r.status_code)
    print(r.json())

# send_put()

def send_patch():
    title = input("Enter Title: ")
    id =1
    user_agent = "Mozilla/5.0 (X11; Linux x86_64; rv:87.0) Gecko/20100101 Firefox/87.0"

    url = f"https://jsonplaceholder.typicode.com/posts/{id}"

    headers = {
        'User-Agent': user_agent
    }
    data = {
        'title': title
    }

    r = requests.patch(url, data=data, headers=headers)
    print(r.status_code)
    print(r.json())

# send_patch()

def send_delete():
    id =1

    url = f"https://jsonplaceholder.typicode.com/posts/{id}"
    r = requests.delete(url)
    print(r.status_code)
    print(r.json())

send_delete()