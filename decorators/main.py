


def decorate_me(func):
    """ func: def"""
    def inner(*args, **kwargs):
        print("~~~~~~~~~~~~~~~~~~~~~~~~~")
        func(*args, **kwargs)
        print("~~~~~~~~~~~~~~~~~~~~~~~~~")
    
    return inner


name = input("Enter name: ")

@decorate_me
def say_name(name):
    print("Hello", name)



# print(a)a(name)

say_name(name)
