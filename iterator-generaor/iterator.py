
# __iter__ __next__

l = [40, 30, 56, 53]
a = iter(l)

next(a) # a is an iteratoor object.
# 40
next(a)
# 30
next(a)
# 56
next(a)
# 53
next(a)
# StopIteration

a = iter(l)

for i in a:
    print(i)

"""
In [37]: a = iter(l)

In [38]: def print_iter(iter):
    ...:     while True:
    ...:         try:
    ...:             value = next(iter)
    ...:             print(value)
    ...:         except StopIteration:
    ...:             break
    ...: 

In [39]: print_iter(a)
40
34
56
76
"""
