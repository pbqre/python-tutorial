

def print_iter(iter):
    while True:
        try:
            value = next(iter)
            print(value)
        except StopIteration:
            break

class Cubic:
    """ 
    1. __iter__
    2. __next__
    """
    def __init__(self, no):
        self.no = no
    
    def __iter__(self):
        self.n = 0
        return self
    
    def __next__(self):
        if self.n < self.no:
            self.n += 1
            return self.n ** 3
        else:
            raise StopIteration


c = Cubic(30)
itc = iter(c)

print_iter(itc)