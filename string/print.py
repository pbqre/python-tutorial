print("Hello", end=" ")
print("World")


a = [
    [2,4,5,6,7,8],
    [2,5,5,7,6,8],
    [87,45,5,6,7,8]
]
row = 3
col = 6

for i in range(row):
    for j in range(col):
        print(a[i][j], end=" ")
    print()
