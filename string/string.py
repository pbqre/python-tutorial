# Three types of string.

# 1. Single qouted string.
# 2. Double Quoted String.
# 3. Triple Qouted String.

a = 'Hello World'
b = "Hello World"
c = """Hello World"""

# \n, \t

# String is also mutable.
a = str()
a = ''

a = "Vivek"

# Accesing a specific element in the string.
print(a[2])

# Operator Overloading.
# When you can use a same operator for more than one purpose.
"""
In [65]: a * 5
Out[65]: 'Hello Hello Hello Hello Hello '
In [66]: a * 10
Out[66]: 'Hello Hello Hello Hello Hello Hello Hello Hello Hello Hello '
"""

# Concatination
"""
In [69]: a + str(45)
Out[69]: 'Hello 45'
"""

# In and not in operator.
"""
In [70]: a = "This is a really long modified string."

In [71]: a
Out[71]: 'This is a really long modified string.'

In [72]: 'long' in a
Out[72]: True

In [73]: 'l' in a
Out[73]: True

In [74]: ' really long' in a
Out[74]: True

In [75]: 'long' in a
Out[75]: True

In [76]: 'long' not in a
Out[76]: False
"""

# Concatenation using parenthesis.
"""
In [82]: a = ('I' 'am' 'a')

In [83]: a
Out[83]: 'Iama'

In [84]: a = 'Hello' 'World'

In [85]: a
Out[85]: 'HelloWorld'

In [86]: a = 'Hello'                  'World'

In [87]: a = 'Hello'                  'World'   ' <<<"'

In [88]: a
Out[88]: 'HelloWorld <<<"'

In [89]: a = 'HEllo''World'

In [90]: a
Out[90]: 'HElloWorld'
"""


# Iterating over the string.
"""
In [95]: for i in range(len(a)):
    ...:     print("Index:", i, "Char:", a[i])
    ...: 
Index: 0 Char: T
Index: 1 Char: h
Index: 2 Char: i
Index: 3 Char: s
"""

# BuilIn functions for string.
# len()
# [(0, 'H'), ...]

"""
In [1]: a = "Hello World"

In [2]: a
Out[2]: 'Hello World'

In [3]: len(a)
Out[3]: 11

In [4]: enumerate(a)
Out[4]: <enumerate at 0x7fc5bbe93400>

In [5]: list(enumerate(a))
Out[5]: 
[(0, 'H'),
 (1, 'e'),
 (2, 'l'),
 (3, 'l'),
 (4, 'o'),
 (5, ' '),
 (6, 'W'),
 (7, 'o'),
 (8, 'r'),
 (9, 'l'),
 (10, 'd')]
"""

# f-string

"""
In [15]: no
Out[15]: 34665

In [16]: salary
Out[16]: 54646.4545

In [17]: a
Out[17]: 'Hello World'

In [18]: s = f"HEllo, I am {no} my salary is {salary}"

In [19]: s
Out[19]: 'HEllo, I am 34665 my salary is 54646.4545'

In [20]: s = "HEllo, I am {no} my salary is {salary}"

In [21]: s
Out[21]: 'HEllo, I am {no} my salary is {salary}'

In [22]: s = f"HEllo, I am {no} my salary is {salary}"

In [23]: s
Out[23]: 'HEllo, I am 34665 my salary is 54646.4545'
"""

"""
In [16]: salary
Out[16]: 54646.4545

In [17]: a
In [29]: no
Out[29]: 34665

In [30]: salary
Out[30]: 54646.4545

In [31]: a
Out[31]: ''

In [32]: a = "My name is {}, and salary is {}".format(no, salary)

In [33]: a
Out[33]: 'My name is 34665, and salary is 54646.4545'

In [34]: a = "My name is {_no}, and salary is {s}".format(_no=no, s=salary)

In [35]: a
Out[35]: 'My name is 34665, and salary is 54646.4545'

In [37]: b = "{0}, {2}, {1}".format("Hello[First]", "World[Second]", "Third")
"""

# .lower() return a string in lowercase.
# .upper() return a string in uppercase.
# .replace(sub_str, new_str) return a new string.
# .replace(sub_str) return the index from where it starts.
