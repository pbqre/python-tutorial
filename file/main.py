# # Talking about file manipulation in python.
# # FILE_NAME = "../FILE.txt" # Relative
# # FILE_NAME = "INPUT.txt"
# # FILE_NAME = "/home/vivekascoder/code/python/FILE.txt" # Absolute.
# FILE_NAME = "INPUT.txt"
# # Reading a file.
# file = open(FILE_NAME, "r+")

# # data = file_p.read()
# # print(data)
# data = file.read()
# inp = input("Enter text: ")
# data += "\n" + inp
# file.write(data)



# file = open("INPUT.txt", "w")

lines = ['Hello Line another example.']
# file.writelines(lines)
# file.close()
# f = open("INPUT.txt", "r")

# print(f.read())

# with open("INPUT.txt", "w") as file:
#     file.writelines(lines)

# with open("INPUT.txt", "r") as file:
#     data = file.read()
#     print(data)

# TODO: binary mode, some of the os funstions.

with open("./INPUT.txt", "w+b") as file:
    file.write(b"This is binary data.")
