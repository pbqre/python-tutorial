# How to declare/create a tuple.
# Mutable:   You can change the value.
# Immutable: You can't change the value.

# List: Mutable.
# Tuple: Immutable.

t = tuple()
t = tuple((3,4))
t = (3,4)
t = tuple([5,6,7,4])
print(t)

#######
# t[2] = 45 # ERROR
#######

"""
In [6]: a = ('vivek', '34')

In [7]: a
Out[7]: ('vivek', '34')

In [8]: b, c = a

In [9]: b
Out[9]: 'vivek'

In [10]: c
Out[10]: '34'

In [11]: c = int(c)

In [12]: c
Out[12]: 34

In [13]: a = (b, c)

In [14]: a
Out[14]: ('vivek', 34)
"""

# in and not in operator..
"""
In [24]: a
Out[24]: (3, 4, 10)

In [25]: 4 in a
Out[25]: True

In [26]: 45 in a
Out[26]: False

In [27]: 4 not in a
Out[27]: False

In [28]: 3434 not in a
Out[28]: True
"""

