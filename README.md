## Python Shells 
- default python
- ipython
  - `pip install ipython`
- bpython
  - `pip install bpython`
- Jupyter Notebook
  - `pip install notebook`

## How to run Jupyter notebook.
- `jupyter notebook`

## Noraml usecases of re.
- Email:
  - `xxxxxx@xxxx.xxxx`
- Phone:
  - `999999999`


## RE
- `[A-Za-z]`
- `[A-Fa-f0-9]` from 0 to 9, a to f, A to F
- `[^A-D]`
- `[^A-D]{4}`
- `[^A-D]{4, 6}`
- `([A-C]{4}|[X-Z]{4,6})` '\^'