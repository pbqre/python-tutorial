
from self import Student

class NewClass:
    a = "vivek"

class NewClass:
    a = "vivek"
    b = "hell"
    c = "whatever"

class NewClass3:
    a = "vivek"
    b = "hell"
    def a(a):
        return "Hello" + a

# class Student:
#     name = ""
#     class_ = 0

# s1 = Student()
# s1.name = "Vivek"
# s1.class_ = 12

# s2 = Student()
# s2.name = "Vishal"
# s2.class_ = 10


# # print(type(s1))
# print("s1", s1.name, s1.class_)
# print("s2", s2.name, s2.class_)

s1 = Student(name="Vivek", class_=23)
# s1.name = "vivek"
a = s1.get_name(34)
print(a)
# s1.class_ = 12
# print(s1.get_name(5))

# print("-->", s1.__name)
print(s1._Student__get_class())
