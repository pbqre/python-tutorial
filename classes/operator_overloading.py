"""
+  => __add__(self, other)
-  => __sub__(self, other)
*  => __mul__(self, other)
/  => __truediv__(self, other)
// => __floordiv__(self, other)
** => __pow__(self, power)
%  => __mod__(self, other)
&  => __and__(self, other)
|  => __or__(self, other)
-  => __invert__(self)
<  => __lt__(self, other)
>  => __gt__(self, other)
== => __eq__(self, other)
<= => __le__(self, other)
>= => __ge__(self, other)

str(object)  => __str__(self)
To represent => __repr__(self)
"""

class Fraction():
    def __init__(self, n, d):
        self.n = n
        self.d = d

    def __add__(self, other):
        temp = Fraction((self.n+other.n), (self.d+other.d))
        return temp
    
    def __str__(self):
        return f"{self.n} / {self.d}"
    
    def __repr__(self):
        return f"{self.n} / {self.d}"

    def print_fraction(self):
        return f"{self.n} / {self.d}"

    def __lt__(self, other):
        return self.n /  self.d  < other.n / other.d
    
    def __gt__(self, other):
        return self.n /  self.d > other.n / other.d
    
    def __eq__(self, other):
        print("I don't wanna compare.")

a = Fraction(10, 7)

b = Fraction(5, 7)
# a + b 
# print((a.n+b.n), '/', (a.d+b.d))

c = a+b
print(c.print_fraction())