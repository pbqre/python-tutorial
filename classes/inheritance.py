



class Rectangle():
    """ Child Class. """
    def __init__(self, l, b):
        print("This is a rectangle Class.")
        self.l = l
        self.b = b
    
    def perimeter(self):
        return 2*(self.l + self.b)
    
    def area(self):
        return self.l * self.b
    
    def issquare(self):
        return self.l == self.b

class Square():
    """ Parent Class """
    def __init__(self, s):
        print("This is a square class.")

        # super().__init__(s, s)
        self.s = s
    
    # def issquare(self, no):
    #     return no * self.s


class PerfectSquare(Rectangle, Square):
    def __init__(self, no):
        print("This is a perfect square constructor.")
        # super().__init__(no)
        Square.__init__(self, no)
        Rectangle.__init__(self, no, no)
    
if __name__ == '__main__':
    # s = Square(10)
    s = PerfectSquare(10)
    print("Square Side: ", s.s)
    print("Length: ", s.l)
    print("Breadth: ", s.b)
    print("Perimeter: ", s.perimeter())
    print("Area: ", s.area())
    print("IsSquare: ", s.issquare())

