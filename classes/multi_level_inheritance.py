# Multi Level Inheritance.

class A:
    pass

class B(A):
    pass

class C(B):
    pass

class D(C):
    pass

