# Demostrating Polymorphism.


class Student():
    def __init__(self, name):
        self.name = name
    
    def say_name(self):
        return f"Hello {self.name}"

class Teacher():
    def __init__(self, name):
        self.name = name

    def say_name(self):
        return f"This is {self.name}, you're teacher."
    
    def name_len(self):
        return len(self.name)


def give_the_name(class_object):
    print(class_object.say_name())



# s = Student("Ram")
# t = Teacher("Sita")
