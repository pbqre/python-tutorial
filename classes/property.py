# TAlking about Property

class KmToMeter():
    def __init__(self, km):
        self._km = km

    @property
    def kilo_meter(self):
        """ Getter """
        return self._km
    
    @kilo_meter.setter
    def kilo_meter(self, new_km):
        """ Setter """
        self._km = new_km

    def to_meter(self):
        return self._km * 1000
    
    # kilo_meter = property(get_km, set_km)



# k = KmToMeter(3.87)
# # k.km = 8.97
# print('Old Value:', k.get_km())
# k.set_km(9.657)
# print(k.to_meter())

# k = KmToMeter(3.87)
# print("Old:", k.kilo_meter)
# k.kilo_meter = 9.456
# print(k.to_meter())

k = KmToMeter(3.87)
print("Old:", k.kilo_meter)
k.kilo_meter = 9.456
print(k.to_meter())