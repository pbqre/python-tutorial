"""
Funtions: 
syntax:

def <name-of-func>():
	statement 1
	statement 2
	...
"""

# def func1(name, age):
# 	"""
# 	This is function is not doing anything.
# 	"""
# 	print(name, age)




# # func1()
# # func1()
# # func1()


# func1("Vivek", 97)


# ===================== 19 -=============

# def say_hello(name, age):
# 	print(name)
# 	print("Age: ", age)


# say_hello("Vivek", 2323)

# ============ 19 =================

# def return_sum(name, _list=[1,2,3]):
# 	"""
# 	This is a nice explaination of the function.
# 	"""
# 	sum = 0

# 	for i in _list:
# 		sum += i

# 	print(name)
# 	return sum


# new_list = [23, 34, 23, 56, 67, 23, 35]

# ans = return_sum(_list=new_list, name="Vivek")

# print("Sum: ", ans)


# =============================


# def return_sum(*args):
# 	"""
# 	This is a nice explaination of the function.
# 	"""
# 	sum = 0

# 	for i in args:
# 		if isinstance(i, str):
# 			if i.isdigit():
# 				i = int(i)
# 			else:
# 				print("Error")
# 				exit(1)

# 		sum += int(i)

# 	return sum


# print(return_sum(1,23,4,5,6, 34, 64, '34'))

# int ban = 34;

# =======================
# a = 10

# def say_hello(name):
# 	global a
# 	a = 12

# 	print("Inside:", a)
# 	if isinstance(name, str):
# 		print("Hello,", name)
# 	else:
# 		print("Not a string")

# # a = 100

# print("Outside:", a)
# say_hello("Vivek")
"""
{
	"a": "value1",
	"b": "value2"
}
"""

# print("After Outside:", a)


# ================ **kwargs =====================

# def a(**kwargs):
# 	i = input("Enter the keyword: ")
# 	if i in kwargs:
# 		print(kwargs[i])
# 	else:
# 		print("Not in keys.")



# a(a=12,b=34, name="Vivek")

# ======== RECURSION ==============
# When a function call itself.

# def a():
# 	a()

# Without recursion.
# def factorial(no):
# 	result = 1

# 	for i in range(1, no + 1):
# 		result *= i # result = result * i
	
# 	return result

# def factorial_recursion(no):
# 	"""
# 	3 * 2 *  1
# 	"""
# 	if no == 1:
# 		return 1
# 	else:
# 		return no * factorial_recursion(no - 1)


# print(factorial(5))
# print(factorial_recursion(5))

# int a = 23;
# Explicit type declaration
i = int()
b = str()
c = bool()

print(type(i))