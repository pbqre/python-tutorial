# Different way to declare sets.
a = {34, 45, 46, 7}
b = set() 
c = set((34, 45, 5, 34))
d = set([2, 4, 5, 6, 7, 6])
e = set({34, 45, 46 , 56})

print(a, type(a))
print(a, b, c, d, e, sep="\n")

a - b # Elements inside a which are not a part of b.
b - a # Elements inside b which are not a part of a.
"""

In [40]: a
Out[40]: {3, 4, 8, 45, 46, 56, 57, 97, 6746, 7687, 8797}

In [41]: b
Out[41]: {3, 4, 10}

In [42]: a & b
Out[42]: {3, 4}

In [43]: a.intersection(b)
Out[43]: {3, 4}

In [44]: b.intersection(a)
Out[44]: {3, 4}

In [45]: a
Out[45]: {3, 4, 8, 45, 46, 56, 57, 97, 6746, 7687, 8797}

In [46]: b
Out[46]: {3, 4, 10}

In [47]: a - b
Out[47]: {8, 45, 46, 56, 57, 97, 6746, 7687, 8797}

In [48]: b - a
Out[48]: {10}
"""

# in and not in operator,
"""
In [92]: a
Out[92]: {2, 3, 4, 5, 6, 7}

In [93]: 6 in a
Out[93]: True

In [94]: 60 in a
Out[94]: False

In [95]: 6 not in a
Out[95]: False

In [96]: 70 not in a
Out[96]: True
"""

# frozenset.
"""
In [101]: a = frozenset({2,3,4,6,7,6,7})

In [102]: a
Out[102]: frozenset({2, 3, 4, 6, 7})

In [103]: type(a)
Out[103]: frozenset

In [104]: a.add()
---------------------------------------------------------------------------
AttributeError                            Traceback (most recent call last)
<ipython-input-104-fddc01c87ea9> in <module>
----> 1 a.add()

AttributeError: 'frozenset' object has no attribute 'add'

In [105]: 
"""


"""
Python
    - int
    - float
    - boolean
    - string
    - list
    - tuple
    - dict
    - set
    - complex
"""

# Complex
"""
In [106]: a = complex()

In [107]: a
Out[107]: 0j

In [108]: a = 2 + 3j

In [109]: a
Out[109]: (2+3j)

In [110]: type(a)
Out[110]: complex

In [111]: b = 4 + 5j

In [112]: a + b
Out[112]: (6+8j)

In [113]: a - b
Out[113]: (-2-2j)

In [114]: a * b
Out[114]: (-7+22j)

In [115]: a / b
Out[115]: (0.5609756097560976+0.0487804878048781j)
"""