# a = [
#     [1,'vivek'],
#     [2, 'Vishal'],
#     [3, 'game'],
#     [4, 'class']
# ]

# for i, j in a:
#     print("The key is:", i, "And value is:", j)

my_dict = {} # empty_dict
my_dict = dict()

# a = {1: 'Vivek', 2: 'Vishal', 3: 'Nothing'}
a = {
    1: 'Vivek', 
    2: 'Vishal', 
    3: {
        'data': [2,3,4,6,8,7]
    },
    'age': 455
}

print(a)

# How to access specific element from the dictionary.

print(a[2])

print("Age:", a['age'])
a.get(2, False) # return the data.

# Changnng and adding elements from a dictionary.
"""

In [44]: a
Out[44]: {1: 'Vivek', 2: 'Vishal', 3: {'data': [2, 3, 4, 6, 8, 7]}, 'age': 455}

In [45]: a[1]
Out[45]: 'Vivek'

In [46]: a[1] = 'None'

In [47]: a
Out[47]: {1: 'None', 2: 'Vishal', 3: {'data': [2, 3, 4, 6, 8, 7]}, 'age': 455}

In [48]: a
Out[48]: {1: 'None', 2: 'Vishal', 3: {'data': [2, 3, 4, 6, 8, 7]}, 'age': 455}

In [49]: a[3] = 0

In [50]: a[3]
Out[50]: 0

In [51]: a
Out[51]: {1: 'None', 2: 'Vishal', 3: 0, 'age': 455}

In [52]: 
"""

# Adding new elements inside the dict.
"""
Out[53]: {1: 'None', 2: 'Vishal', 3: 0, 'age': 455}

In [54]: # 'language' : 'python'

In [55]: a['language'] = 'python'

In [56]: a['language']
Out[56]: 'python'

In [57]: a.get('language', False)
Out[57]: 'python'

In [58]: a
Out[58]: {1: 'None', 2: 'Vishal', 3: 0, 'age': 455, 'language': 'python'}

In [59]: a[5] = 'secret string'

In [60]: a
Out[60]: 
{1: 'None',
 2: 'Vishal',
 3: 0,
 'age': 455,
 'language': 'python',
 5: 'secret string'}

In [61]: 
"""

## Deleting the whole dict.
# del name_of_dict



# Removing elements from the dictionary.


# dict.pop(key)
## Removes the key value pair and return the value.

# dict.popitem()
## Removes the last key, value pair from a dict and return
## (key, value)

# Dict comprehension
a = {x: x**2 for x in range(10)}

## fromkeys(interable, default)
"""
In [80]: d
Out[80]: {}

In [81]: type(d)
Out[81]: dict

In [82]: a
Out[82]: ['bmw', 'ferrari', 'toyota', 'city']

In [83]: d.fromkeys(a, 100000)
Out[83]: {'bmw': 100000, 'ferrari': 100000, 'toyota': 100000, 'city': 100000}
"""

## .keys() and .values()

"""
In [88]: d
Out[88]: {'bmw': 100000, 'ferrari': 100000, 'toyota': 100000, 'city': 100000}

In [89]: d.keys()
Out[89]: dict_keys(['bmw', 'ferrari', 'toyota', 'city'])

In [90]: keys = d.keys()

In [91]: keys
Out[91]: dict_keys(['bmw', 'ferrari', 'toyota', 'city'])

In [92]: list(keys)
Out[92]: ['bmw', 'ferrari', 'toyota', 'city']

In [93]: d.values()
Out[93]: dict_values([100000, 100000, 100000, 100000])

In [94]: values = d.values()

In [95]: list(values)
Out[95]: [100000, 100000, 100000, 100000]
"""

# .clear()

"""
In [96]: d
Out[96]: {'bmw': 100000, 'ferrari': 100000, 'toyota': 100000, 'city': 100000}

In [97]: d.clear()

In [98]: d
Out[98]: {}
"""

# in and not in
"""
In [102]: d
Out[102]: {'bmw': 10000, 'ferrari': 10000, 'toyota': 10000, 'city': 10000}

In [103]: 'bmw' in d
Out[103]: True

In [104]: 10000 in d
Out[104]: False

In [105]: 'sdfs' in d
Out[105]: False

In [106]: 'city' in d
Out[106]: True

In [107]: 'city' not in d
Out[107]: False
"""

## Iterating over the dictionary.
# for i in d:
#     print(i)

"""
In [108]: for i in d:
     ...:     print(i)
     ...: 
bmw
ferrari
toyota
city

In [109]: for i in d:
     ...:     print("Index is:", i, "Value is:", d[i])
     ...: 
     ...: 
Index is: bmw Value is: 10000
Index is: ferrari Value is: 10000
Index is: toyota Value is: 10000
Index is: city Value is: 10000

In [110]: d
Out[110]: {'bmw': 10000, 'ferrari': 10000, 'toyota': 10000, 'city': 10000}

In [111]: 
"""

# all(), any(), len()

# -> all()
"""
In [18]: a = {
    ...: 2: "hello",
    ...: 3: 'sfsf',
    ...: 0: "Zero"
    ...: }

In [19]: a
Out[19]: {2: 'hello', 3: 'sfsf', 0: 'Zero'}

In [20]: len(a)
Out[20]: 3

In [21]: all(a)
Out[21]: False
"""

# -> any()
"""
In [19]: a
Out[19]: {2: 'hello', 3: 'sfsf', 0: 'Zero'}

In [20]: len(a)
Out[20]: 3

In [21]: all(a)
Out[21]: False

In [22]: any(a)
Out[22]: True

In [23]: a = {0: 23, 4: 5, 0: 45}

In [24]: a
Out[24]: {0: 45, 4: 5}

In [25]: any(a)
Out[25]: True
"""
