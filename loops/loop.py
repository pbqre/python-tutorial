"""
Loops in python.

Syntax:
while <Condition>:
	statement1 ...
	statement2 ...
	...


syntax:
for <variable> in <iterable object>:
	statement 1...
	statement 2...
	...
"""

a = [12, 34, 45, 5, 23, 35, 76, 2, 56, 5]
# b = "Vivek's Computer"

# Iterating over a list.
# for i in a:
# 	print(i)

# Nested Loop
# for i in a:
# 	print(i)
# 	for i in range(1, i+1):
# 		print("   >>> ", i)


# Iterate over string.
# for i in b:
# 	print(i)

# for i in range(len(b)):
# 	print(b[i])

i = 0
b = "Vivek's Computer"
while i < len(b):
	if b[i] == 'C':
		i += 1
		continue

	print(b[i])
	i = i + 1 # i+=1

print("Ending of the loop")


i = 0
b = "Vivek's Computer"
while i < len(b):
	if b[i] == 'C':
		break

	print(b[i])
	i = i + 1 # i+=1

print("Ending of the loop")