# Assignment of if-else conditions.

## Questions.
- Write a program to take input of two numbers, compare them and print the largest.

- Write a program to take input of three numbers, compare them and print the largest.

- Take values of length and breadth of a rectangle from user and check if it is square or not.

- A shop will give discount of 10% if the cost of purchased quantity is  more than 1000.
Ask user for quantity Suppose, one unit will cost 100. Judge and print total cost for user.

- A school has following rules for grading system:
a. Below 25 - F
b. 25 to 45 - E
c. 45 to 50 - D
d. 50 to 60 - C
e. 60 to 80 - B
f. Above 80 - A
Ask user to enter marks and print the corresponding grade.

- Write a program to print absolute vlaue of a number entered by user. E.g.-
INPUT: 1        OUTPUT: 1
INPUT: -1        OUTPUT: 1
