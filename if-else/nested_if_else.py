"""
Nested if else conditions.
"""

# Comparing three numbers.

a = int(input("Enter first no: "))
b = int(input("Enter second no: "))
c = int(input("Enter third no: "))

if a > b:
	# A is bigger than b
	if a > c:
		print("A is largest.")
		# a is largest.
if b > a:
	# A is bigger than b
	if b > c:
		print("B is largest.")
		# a is largest.
if c > a:
	# A is bigger than b
	if c > b:
		print("C is largest.")
		# a is largest.
