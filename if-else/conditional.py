"""
Conditional Operator.
Docstring.
Today we're talking about conditional operators.'
"""

# If Conditions

# a = 'vivek'
# b = 12
# c = 15

# # if statement start.
# if b <= c:
# 	print("b is equal to c.")
# 	print("....End......")

# # if statement end.

# print("The program died...")

# # Ctrl-/

# If-Else Statements.



# # If-else

# if b >= c:
# 	print("Looks like c is bigger.")
# 	# .....

# else:
# 	print("Hmm! I don't get it.")
# 	# ....
# 	v = "vivek"
# 	print(v)

# # If-else end.


# If-elif-else Conditions.

a = input("Enter name: ") # sita

if a == "vivek" or a == "ram":
	print("Your name is vivek or ram.")

# elif a == "ram":
# 	print("Your name is ram.")

elif a == "sita" or a == "Sita":
	print("Your name is sita.")

else:
	print("I can't spell your name.")




# TODO: d/f b/w " and '