# Various ways to declare a list.


a = [1, 2, 3, 4]
b = []
b = list()

b = list([1,3,4,6]) # list
b = list((1,3,4,6)) # tuple
b = list({1,3,4,6}) # set
a = [2, True, 2.3, 'sfs']
c = list({'a': 'b', 'c': 'd'}) # dictionary.

# Generate a list of no from 1 to 100
c = [x for x in range(1, 101)] # List Comprehension.

# print(c)

# Generate a list of squares no from 1 to 100
c = [x ** 2 for x in range(1, 101)]

# print(c)


# Generate a list of str of no from 1 to 100
c = [str(x) for x in range(1, 101)]

# print(c)


# Different operations of list.

## 1. Append (Adding to the end of the list.)
a = [3,4,5]

a.append(100)


## 2. extend (Merging two list.)
a = [3,4,5]
b = [6,7,8,9]

a.extend(b) # [3, 4, 5, 6, 7, 8, 9]

## 3. insert(index, element)
a = [3,4,5]
a.insert(1, 23) # [3, 23, 4, 5]

## 4. pop(index=last_elements) (removing a element.)
a = [3,4,5]
a.pop() # [3, 4, 5]

## 5. The problem
a = [3,4,5]
b = a
a.pop() # [3, 4]
print(b) # [3, 4]

# Solution:
a = [3,4,5]
b = a.copy()
a.pop() # [3, 4]
print(b) # [3, 4, 5]

## 6. a.index(no) 
### Return index corresponds to a element.



# Positive indexing 
a = [23, 45, 567, 67, 34, 6, 767, 34, 45, 567, 67, 78]
a[1] ## 45
a[len(a) - 1] ## 78
# Negative indexing.
a[-1] ## 78

# Various ways to implement slicing 
a[3:5]
a[-4:-1]
a[::-1] # This will reverse the list.
a[1:]
a[:6]
a[:] # return the whole list.

# Iterating list.
list_name = [x**2 for x in range(1,11)]
for i in list_name:
    print(i)

for i in range(len(list_name)):
    print("Index:", i, "Value:", list_name[i])

# Changing elements in the list.

"""
In [63]: list_name
Out[63]: [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]

In [64]: list_name.index(49)
Out[64]: 6

In [65]: list_name[6] = 94

In [66]: list_name
Out[66]: [1, 4, 9, 16, 25, 36, 94, 64, 81, 100]

In [67]: 
"""
